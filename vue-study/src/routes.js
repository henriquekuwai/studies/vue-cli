const Cadastro = () => System.import('./components/cadastro/Cadastro.vue');
import Home from './components/home/Home.vue';

export const routes = [
    {
        name: 'home',
        title: 'Home',
        path: '',
        component: Home,
        menu: true
    },
    {
        name: 'cadastrar',
        title: 'Cadastro',
        path: '/cadastro',
        component: Cadastro,
        menu: true
    },
    {
        name: 'alterar',
        title: 'Alterar',
        path: '/cadastro/:id',
        title: '',
        component: Cadastro,
        menu: false
    },
    {
        path: '*',
        component: Home,
        menu: false
    }
];