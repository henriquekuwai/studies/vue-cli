import Vue from 'vue';

Vue.directive('meu-transform', {
    bind(el, binding, vnode) {
        let current = 0;

        el.addEventListener('click', () => {
            let incremento = (binding.value || {}).incremento || 90;
            let efeito;

            if (!binding.arg || binding.arg == 'rotate') {
                current += incremento;
                efeito = `rotate(${current}deg)`;
            } else if (binding.arg == 'scale') {
                if (!el.classList.contains('ampliado')) {
                    efeito = `scale(${incremento})`;
                    el.classList.add('ampliado');
                } else {
                    efeito = `scale(1)`;
                    el.classList.remove('ampliado');
                }
            }

            el.style.transform = efeito;
        });
    }
});